include fennel.mk

$(BIN_DIR)/mpvc: src/mpvc.fnl src/commands.fnl
	fennel $(CFLAGS) --compile-binary $< $@ $(LUA_LIB_PATH) $(LUA_INCLUDE_PATH)

.PHONY: test
test: test/init.fnl
	@fennel $<

install:
	cp $(BIN_DIR)/mpvc $(INSTALLATION_DIR)

.DEFAULT_GOAL := bin/mpvc
