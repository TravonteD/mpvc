# MPVC
A commandline interface to the mpv media player

_Note: mpv supports a **TON** of commands so I'm adding them as they become useful_

## Installation

### Prerequisites
- luarocks
- luajit
- mpv: configured with `input-ipc-server=/tmp/mpvsocket` either as a flag or in the `~/.config/mpv/mpv.conf`

### Steps
- Run `make` from the root directory. 
  This will install the depencies using luarocks 
  Note: dependencies are installed using the --local flag by default.
  If you want to use the global dir remove the option in the makefile.
- Run `make install` and a copy of the executable will be placed in `~/.local/bin/` by default.
  You may want this to be `/usr/local/bin`. If so, adjust the variable in the makefile accordingly.
  
## Usage

```
mpvc - a commandline interface for mpv media player
Usage: mpvc <command> <args>
List of commands: 
play-toggle
stop
prev
play
seek-forward
next-chapter
pause
current-playlist
next
fullscreen-toggle
loadfile
current
seek-back
add
chapters
prev-chapter
For more info on the commands reference the mpv manual. man mpv(1)
To request a command that is not listed be added please visit https://github.com/travonted/mpvc
```

### Example

- Playing a file: `add` will append and `loadfile` will replace the existing playlist.

```
mpvc loadfile <path-to-file>
mpvc add <path-to-file>
```
  
