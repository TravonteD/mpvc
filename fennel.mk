CFLAGS = --require-as-include --lua luajit
LUA_LIB_PATH = /usr/lib/libluajit-5.1.so
LUA_INCLUDE_PATH = /usr/include/luajit-2.0
DIRS = src/ test/ bin/

BIN_DIR=bin
INSTALLATION_DIR=~/.local/bin
VPATH=src

%.lua: %.fnl
	fennel $(CFLAGS) --compile $< > $@

.PHONY: repl
repl:
	fennel

new: $(DIRS)
