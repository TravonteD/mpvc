
; vim:ft=fennel
; MPV Control is a command line utility for sending commands to mpv
; via the ipc protocol. For this to work mpv must have the 
; input-ipc-server=/tmp/mpvsocket option enabled
;
; man mpv(1)

(λ param-error [command param]
  {:action :print
   :data (string.format "Error: The %s command requires the parameter [%s]" command param)})

(λ print-help [commands]
  {:action :print
   :data (table.concat 
           ["mpvc - a commandline interface for mpv media player"
            "Usage: mpvc <command> <args>"
            "List of commands: "
            (table.concat (icollect [command _ (pairs commands)] command) "\n")
            "For more info on the commands reference the mpv manual. man mpv(1)"
            "To request a command that is not listed be added please visit https://github.com/travonted/mpvc"
            ] "\n")})


(λ main [args]
  "Runs the given command with the given parameters.
  Only a single command is allowed at a time."

  (local mpvc-commands (require :src.commands))

  (λ send-command [command]
    (let [json (require :json)
          socket (require :socket)]
      (with-open [connection (assert ((require :socket.unix)))]
                 (local response (coroutine.create
                                   #(while true
                                      (let [response (assert (connection:receive "*l"))
                                            parsed_response (json.decode response)]
                                        (if (= 0 parsed_response.request_id)
                                          (coroutine.yield parsed_response.data))))))
                 (assert (connection:connect  (or (os.getenv "MPV_SOCKET") "/tmp/mpvsocket")) "unable to connect to mpv")
                 (assert (connection:send (.. (json.encode command) "\n")))
                 (let [(ok? result) (coroutine.resume response)]
                   result))))

  (λ no-mpv-instance-exists? []
    (not= 0 (os.execute "pgrep 'mpv$' >/dev/null")))

  (λ run [commands]
    (local actions {:send send-command
                    :error #(do (print $1) (os.exit 1))
                    :print print})
    (match commands
      {: action : data} ((. actions action) data)
      {: bindings :cb cb} (-> (collect [k v (pairs bindings)]
                                       (values k (run v)))
                              (cb)
                              (run))))

  (fn safe-call [command param]
    (->  (if
           (not (. mpvc-commands command)) {:action :error
                                            :data (string.format "Error: invalid command %s" command)}
           (no-mpv-instance-exists?) {:action :error
                                      :data "Error: mpv is not running"}
           ((. mpvc-commands command) param))
        (run)))

  (λ safe-play [file command]
    (if (no-mpv-instance-exists?)
      (os.execute (string.format "mpv --quiet %s" file))
      (safe-call command file)))

  (match args
    [:help] (-> mpvc-commands
                (print-help)
                (run))
    [:add file] (safe-play file :add)
    [:loadfile file] (safe-play file :loadfile)
    [:add] (run (param-error :loadfile :file))
    [:loadfile] (run (param-error :loadfile :file))
    [command param] (safe-call command param)
    [command] (safe-call command)
    [] (safe-call :current))
  nil)
(main arg)
