(local commands {})
(λ mpv-do [...]
  {:command [...]})

(λ mpv-set [property value]
  (mpv-do :set_property property value))

(λ mpv-get [property]
  (mpv-do :get_property property))

(λ send-command [data]
  {:action :send
   :data data})

(λ time-format [seconds]
  (let [minutes (math.floor (/ seconds 60))
        hours (math.floor (/ minutes 60))
        seconds (math.floor (% seconds 60))]
    (string.format "%.2d:%.2d:%.2d" hours minutes seconds)))

(λ commands.play []
  (send-command (mpv-set :pause false)))
(λ commands.pause []
  (send-command (mpv-set :pause true)))
(λ commands.stop []
  (send-command (mpv-do :stop)))
(λ commands.next []
  (send-command (mpv-do :playlist-next)))
(λ commands.prev []
  (send-command (mpv-do :playlist-prev)))
(λ commands.seek-back []
  (send-command (mpv-do :seek -30)))
(λ commands.seek-forward []
  (send-command (mpv-do :seek 30)))
(λ commands.loadfile [file]
  (send-command (mpv-do :loadfile file)))
(λ commands.add [file]
  (send-command (mpv-do :loadfile file :append)))
(λ commands.play-toggle []
  {:action :send
   :bindings {:play-status {:action :send
                            :data (mpv-get :pause)}}
   :cb (λ [tbl] 
         {:action :send
          :data (mpv-set :pause (not tbl.play-status))})})
(λ commands.fullscreen-toggle []
  {:action :send
   :bindings {:fs {:action :send
                   :data (mpv-get :fs)}}
   :cb (λ [tbl] 
         {:action :send
          :data (mpv-set :fs (not tbl.fs))})})

(λ commands.format-playlist [{: playlist}]
  {:action :print 
   :data (-> (icollect [_ item (ipairs playlist)]
                       (match item
                         {:current true :filename name} (.. "* " name)
                         {:current _ :filename name} (.. "  " name)))
             (table.concat "\n"))})

(λ commands.current-playlist []
  {:action :print
   :bindings {:playlist {:action :send
                         :data (mpv-get :playlist)}}
   :cb commands.format-playlist})

(λ commands.format-chapters [{: chapters}]
  {:action :print
   :data (-> (icollect [i item (ipairs chapters)]
                       (string.format "%s %s %s" 
                                      i 
                                      (time-format item.time) 
                                      item.title))
             (table.concat "\n"))})

(λ commands.chapters []
  {:action :print
   :bindings {:chapters {:action :send
                         :data (mpv-get :chapter-list)}}
   :cb commands.format-chapters})

(λ commands.calc-next-chapter [{: chapter-count : current-chapter}]
  "Calculates the previous chapter. Chapters from mpv are zero indexed"
  {:action :send
   :data (mpv-set :chapter (if (= current-chapter (- chapter-count 1))
                             current-chapter
                             (+ current-chapter 1)))})

(λ commands.next-chapter []
  {:action :send
   :bindings {:chapter-count {:action :send
                              :data (mpv-get :chapters)}
              :current-chapter {:action :send 
                                :data (mpv-get :chapter)}}
   :cb commands.calc-next-chapter})

(λ commands.prev-chapter []
  {:action :send
   :bindings {:chapter-count {:action :send 
                              :data (mpv-get :chapters)}
              :current-chapter {:action :send 
                                :data (mpv-get :chapter)}}
   :cb commands.calc-prev-chapter})

(λ commands.calc-prev-chapter [{: chapter-count : current-chapter}]
  "Calculates the next chapter. Chapters from mpv are zero indexed"
  {:action :send
   :data (mpv-set :chapter (if (> current-chapter 0)
                             (- current-chapter 1)
                             0))})

(λ commands.format-current-info [{: title
                                  : play-status
                                  : playlist-pos
                                  : playlist-count
                                  : time-pos
                                  : time-remaining
                                  : percent-pos}]
  {:action :print 
   :data (string.format "%s\n[%s] #%d/%d %s/%s (%d%%)"
                        title
                        (if play-status "paused" "playing")
                        (if playlist-count playlist-pos)
                        (if playlist-count playlist-count)
                        (if time-pos (time-format time-pos))
                        (if time-remaining (time-format time-remaining))
                        (if percent-pos (math.floor percent-pos)))})


(λ commands.current []
  {:bindings {:title {:action :send
                      :data (mpv-get :media-title)}
              :play-status {:action :send
                            :data (mpv-get :pause)}
              :playlist-pos {:action :send
                             :data (mpv-get :playlist-pos-1)}
              :playlist-count {:action :send
                               :data (mpv-get :playlist-count)}
              :time-pos {:action :send
                         :data (mpv-get :time-pos)}
              :time-remaining {:action :send
                               :data (mpv-get :time-remaining)}
              :percent-pos {:action :send
                            :data (mpv-get :percent-pos)}}
   :cb commands.format-current-info})

(λ commands.chapter [chapter]
  (send-command (mpv-set :chapter (- chapter 1))))

(λ commands.speed [speed]
  (send-command (mpv-set :speed speed)))

(λ commands.reload []
  (send-command (mpv-do :video-reload)))

commands
