(require-macros :test.util)
(local lu (require :luaunit))

; Helpers
(local view (require :fennelview))
(local cmds (require :src.commands))

(deftest :play
  (lu.assertEquals (match (cmds.play) 
                     {:action :send :data {:command [:set_property :pause false]}} true)
                   true))
(deftest :pause
  (lu.assertEquals (match (cmds.pause) 
                     {:action :send :data {:command [:set_property :pause true]}} true) 
                   true))
(deftest :stop
  (lu.assertEquals (match (cmds.stop) 
                     {:action :send :data {:command [:stop]}} true) 
                   true))
(deftest :next
  (lu.assertEquals (match (cmds.next) 
                     {:action :send :data {:command [:playlist-next]}} true) 
                   true))
(deftest :prev
  (lu.assertEquals (match (cmds.prev) 
                     {:action :send :data {:command [:playlist-prev]}} true) 
                   true))
(deftest :seek-forward
  (lu.assertEquals (match (cmds.seek-forward) 
                     {:action :send :data {:command [:seek 30]}} true) 
                   true))
(deftest :seek-back
  (lu.assertEquals (match (cmds.seek-back) 
                     {:action :send :data {:command [:seek -30]}} true) 
                   true))
(deftest :loadfile
  (lu.assertEquals (match (cmds.loadfile :test-file)
                     {:action :send :data {:command [:loadfile :test-file]}} true) true))
(deftest :add
  (lu.assertEquals (match (cmds.add :test-file)
                     {:action :send :data {:command [:loadfile :test-file :append]}} true) 
                   true))

(deftest :play-toggle
  (lu.assertEquals (match (cmds.play-toggle)
                     {:bindings {: play-status}
                      :cb cb} true)
                   true))

(deftest :fullscreen-toggle
  (lu.assertEquals (match (cmds.fullscreen-toggle)
                     {:bindings {: fs}
                      :cb cb} true)
                   true))

(deftest :current-playlist
  (lu.assertEquals 
    (match (cmds.current-playlist) 
      {:bindings {: playlist}
       :cb cb} true)
    true))

(deftest :format-playlist
  (lu.assertEquals
    (cmds.format-playlist {:playlist [{:current false :filename :test1}
                                      {:current true :filename :test2}]})
    {:action :print
     :data "  test1\n* test2"}))

(deftest :format-chapters
  (lu.assertEquals
    (cmds.format-chapters {:chapters [{:title :ch1 :time 1}
                                      {:title :ch2 :time 2}]})
    {:action :print
     :data "1 00:00:01 ch1\n2 00:00:02 ch2"}))

(deftest :chapters
  (lu.assertEquals 
    (match (cmds.chapters) 
      {:bindings {: chapters}
       :cb cb} true)
    true))

(deftest :next-chapter
  (lu.assertEquals 
    (match (cmds.next-chapter) 
      {:bindings {: chapter-count
                  : current-chapter}
       :cb cb} true)
    true))

(deftest :calc-next-chapter
  (lu.assertEquals
    (cmds.calc-next-chapter {:chapter-count 2
                             :current-chapter 0})
    {:action :send
     :data {:command [:set_property :chapter 1]}})
  (lu.assertEquals
    (cmds.calc-next-chapter {:chapter-count 2
                             :current-chapter 1})
    {:action :send
     :data {:command [:set_property :chapter 1]}}))

(deftest :prev-chapter
  (lu.assertEquals 
    (match (cmds.prev-chapter) 
      {:bindings {: chapter-count
                  : current-chapter}
       :cb cb} true)
    true))

(deftest :calc-prev-chapter
  (lu.assertEquals
    (cmds.calc-prev-chapter {:chapter-count 2
                             :current-chapter 1})
    {:action :send
     :data {:command [:set_property :chapter 0]}})
  (lu.assertEquals
    (cmds.calc-prev-chapter {:chapter-count 2
                             :current-chapter 0})
    {:action :send
     :data {:command [:set_property :chapter 0]}}))

(deftest :format-current-info
  (lu.assertEquals
    (cmds.format-current-info {:title :test-title
                               :play-status true
                               :playlist-pos 1
                               :playlist-count 2
                               :time-pos 1
                               :time-remaining 5
                               :percent-pos 20})
    {:action :print
     :data "test-title\n[paused] #1/2 00:00:01/00:00:05 (20%)"}))

(deftest :current
  (lu.assertEquals 
    (match (cmds.current) 
      {:bindings {: title
                  : play-status
                  : playlist-pos
                  : playlist-count
                  : time-pos
                  : time-remaining
                  : percent-pos}
       :cb cb} true)
    true))

(deftest :chapter
  (lu.assertEquals 
    (match (cmds.chapter 2)
      {:action :send
       :data {:command [:set_property :chapter 1]}})))

(deftest :speed
  (lu.assertEquals 
    (match (cmds.speed 2)
      {:action :send
       :data {:command [:set_property :speed 2]}})))
