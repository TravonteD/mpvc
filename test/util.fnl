(fn deftest [name ...]
  `(tset _G ,(.. "test-" name) (λ [] ,...)))
{: deftest}
